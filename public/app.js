function scrollDown() {
  const v = $("#encryptPage").offset().top;
  $("html, body")
    .stop()
    .animate(
      {
        scrollTop: v
      },
      700
    );
}

$(document).ready(function() {
  $("#banner")
    .hide(0)
    .delay(800)
    .fadeIn(1100);
  $("#icon")
    .delay(800)
    .animate({ zoom: "102%" }, 1100);
  $("#title")
    .delay(800)
    .animate({ zoom: "102%" }, 1100);
  $("#bullet")
    .hide(0)
    .delay(3000)
    .fadeIn(1100);
});
